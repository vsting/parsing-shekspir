module PagesLoader
  def load(url)
    page = false
    begin
      page = Nokogiri::HTML(Net::HTTP.get(URI.parse(URI.escape("#{url.to_s}"))))
    rescue Exception => e
      puts "Error CONNECT on #{url}, message: #{e.message}"
    end
    page
  end
end

module Parser
  def works(page)
    page.css('td[valign="BASELINE"]')
  end

  def parts(page)
    parts = []
    raw_parts = page.css('tr[align="CENTER"] td h2')
    raw_parts.each_with_index do |raw_part, index|
      parts << raw_part.text.delete("\n")
    end
    parts
  end
end

module Threading
  include PagesLoader

  def load_pages_in_thread(urls:)
    @result, threads, pages = [], [], urls

    pages.each do |page_to_fetch|
      threads << Thread.new(page_to_fetch) do |url|
        if page = load(url)
          @result << page
        end
      end
    end

    threads.each {|thr| thr.join }
    @result
  end

  def load_by_pool_urls(urls)
    until (pool_urls = urls.slice!(0..99)).empty?
      @pages = load_pages_in_thread(urls: pool_urls)
    end
    @pages
  end
end

class Shekspir
  include Parser
  include Threading

  def initialize(url)
    @url = url
  end
end